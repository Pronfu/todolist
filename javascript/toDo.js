/* This is the javascript for the to do's, anything you change here will affect how the to do's operate. */

const addneedDo = document.querySelector('.add-needDo');
const needDoList = document.querySelector('.toDo');
/* Storing all the data in the needDo array. After page refresh check to see if there is anything in LocalStorage if not then laod an empty list.  */
const needDo = JSON.parse(localStorage.getItem('needDo')) || [];

function addNeedDo(e)
{
    e.preventDefault();
    /* Getting the textbox item (named item) to be put into the list. Since this is all client-side then need to get the item from the same page.  */
    const text = (this.querySelector('[name=item]')).value;
    const item = 
    {
        text: text, /* If you know ES6 you could just do text but I wanted to lay it out to be easy */
        done: false
    };

    /* Move the item from the textbox into the needDo array */
    needDo.push(item);

    /* Calling the populateList function to create the list */
    populateList(needDo, needDoList);

    /* Keeping the list after you refresh the page */
    localStorage.setItem('needDo', JSON.stringify(needDo));

    /* Resets the textbox once hit */
    this.reset();
}

/* Each time is is run it recreates one giant list. You could look into figuring out how add just the one item that is added, rather than recreate the entire list using something like React.  */
function populateList(toDo = [], toDoList)
{
    /* Loop over every item in needDo and map it to return a string and each own item is going to be a label.  */
    toDoList.innerHTML = toDo.map((plate, i) => 
    {
        return`
        <li>
        <input type="checkbox" data-index=${i} id="item${i}" ${plate.done ? 'checked' : ''} />
        <label for="item${i}">${plate.text}</label>
        </li>
        `;
      /* The join is going to put it all into one big string */
    }).join('');
}

/* Basicallly to actually check a checkbox and keep it across page refreshes and in localStorage */
function toggleDone(e)
{
    if(!e.target.matches('input')) return; // skip this unless it's an input / checkbox
    const el = e.target;
    const index = el.dataset.index;
    // Basically the opposite, if it's true then return false (if false return true)
    needDo[index].done = !needDo[index].done;
    /* Keeping the list after you refresh the page */
    localStorage.setItem('needDo', JSON.stringify(needDo));
    /* Loading the list that is in LocalStorage. */
    populateList(needDo, needDoList);
}

/* Listening for when the user clicks on the submit button and to addNeedDo */
addneedDo.addEventListener('submit', addNeedDo);

/* Listen for a click on anything inside of needDoList */
needDoList.addEventListener('click', toggleDone);

/* Loading the list that is in LocalStorage on load of the page */
populateList(needDo, needDoList);

/* End all Javascript */