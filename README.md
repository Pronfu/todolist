# To Do List

A todo list that uses localStorage so you can store your todo's in safe keeping (unless you remove localStorage)

## Installation

Either copy or clone this repo then put all the files into the one folder. Then copy that folder to where you what the site to be live, then open that live site in your browser.

## Live Demo

[https://projects.gregoryhammond.ca/todolist/](https://projects.gregoryhammond.ca/todolist/)

## Huge Thanks

Huge thanks goes to [Wes Bos](https://wesbos.com/) and his [Javascript 30](https://javascript30.com/) for giving me the template for this. Without that I would be lost.


## Support Me

This is done in my spare time so if you can afford to give a dollar please either [buy me a coffee](https://www.buymeacoffee.com/gregoryhca), or [pay using Paypal](https://paypal.me/ghammondca).

## License
[Unlicense](https://unlicense.org/)